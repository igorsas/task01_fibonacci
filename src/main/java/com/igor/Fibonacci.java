/*
 * Fibonacci.java
 *
 * 22/01/2019
 *
 * version 1.0
 */

package com.igor;

import java.util.ArrayList;
import java.util.Scanner;
import static java.util.Collections.max;

/**
 * There is a class for finding percentage of odd and even Fibonacci numbers
 *
 * @author Igor Sas
 * @version 1.0
 * @since 2019-01-22
 */

public class Fibonacci {
    /**
     * This method print sum odd and even number and finding percentage of odd and even Fibonacci numbers
     * @param args not using
     */
    public static void main(String[] args) {
        final int NUMBER_100 = 100;
        Scanner scanner = new Scanner(System.in);
        int leftEdge = getLeftEdge(scanner);
        int rightEdge = getRightEdge(scanner, leftEdge);

        ArrayList<Integer> oddNumbers = getOddNumbers(leftEdge, rightEdge);
        ArrayList<Integer> evenNumbers = getEvenNumbers(leftEdge, rightEdge);

        System.out.println("\nSum odd numbers: " + getSumNumbers(oddNumbers));
        System.out.println("Sum even numbers: " + getSumNumbers(evenNumbers));

        int F1 = max(oddNumbers);
        int F2 = max(evenNumbers);
        oddNumbers = new ArrayList<Integer>();
        evenNumbers = new ArrayList<Integer>();
        oddNumbers.add(F1);
        evenNumbers.add(F2);
        int countNumbers;
        do {
            System.out.println("Enter count of fibonacci numbers:");
            countNumbers = scanner.nextInt();
        } while (countNumbers < 3);

        for (int i = 2; i < countNumbers; i++) {
            F2 += F1;
            F1 = F2 - F1;
            if (F2 % 2 == 0) {
                evenNumbers.add(F2);
            } else {
                oddNumbers.add(F2);
            }
        }
        System.out.println("Percent odd numbers on the fibonacci numbers: " + ((((double) oddNumbers.size()) / countNumbers) * NUMBER_100));
        System.out.println("Percent even numbers on the fibonacci numbers: " + (((double) evenNumbers.size()) / countNumbers) * NUMBER_100);
    }

    /**
     * This method asks the user to give the left edge
     * @param scanner Scanner object for input data from console
     * @return left edge
     */
    private static int getLeftEdge(final Scanner scanner) {
        System.out.println("Enter left edge: ");
        return scanner.nextInt();
    }

    /**
     * This method asks the user to give the right edge
     * @param scanner object for input data from console
     * @param leftEdge integer for checking right edge
     * @return right edge
     */
    private static int getRightEdge(final Scanner scanner, final int leftEdge) {
        int rightEdge;
        do {
            System.out.println("Enter right edge: ");
            rightEdge = scanner.nextInt();
        } while (rightEdge < 0 || rightEdge <= leftEdge);
        return rightEdge;
    }

    /**
     * This method print odd numbers from the interval and put them in the ArrayList
     * @param leftEdge integer
     * @param rightEdge integer
     * @return all odd numbers from the interval
     */
    private static ArrayList<Integer> getOddNumbers(final int leftEdge,
                                                    final int rightEdge) {
        ArrayList<Integer> oddNumbers = new ArrayList<Integer>();
        System.out.println("Odd numbers from the start:");
        for (int i = leftEdge; i <= rightEdge; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
                oddNumbers.add(i);
            }
        }
        return oddNumbers;
    }

    /**
     * This method print even numbers from the interval and put them in the ArrayList
     * @param leftEdge integer
     * @param rightEdge integer
     * @return all even numbers from the interval
     */
    private static ArrayList<Integer> getEvenNumbers(final int leftEdge,
                                                     final int rightEdge) {
        ArrayList<Integer> evenNumbers = new ArrayList<Integer>();
        System.out.println("\nEven numbers from the end:");
        for (int i = rightEdge; i >= leftEdge; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
                evenNumbers.add(i);
            }
        }
        return evenNumbers;
    }

    /**
     * This method counts numbers from the list
     * @param numbers for finding sum this numbers
     * @return sum numbers from the list
     */
    private static Integer getSumNumbers(final ArrayList<Integer> numbers) {
        Integer sumNumbers = null;
        for (Integer i : numbers) {
            if (sumNumbers == null) {
                sumNumbers = i;
            } else {
                sumNumbers += i;
            }
        }
        return sumNumbers;
    }
}
